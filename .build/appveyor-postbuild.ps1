<#
.SYNOPSIS

Uploads xUnit Xml test reults to AppVeyor.
.DESCRIPTION

Expects the build properties are stored in the same folder as "msbuild-properties.xml":

<?xml version="1.0" encoding="utf-8"?>
<Properties>
  <MSBuildToolsPath>C:\Program Files (x86)\MSBuild\12.0\bin</MSBuildToolsPath>
  <XUnitTestResults>K:\FullPathToBuild\artifacts\xunit-results.xml</XUnitTestResults>
  <APPVEYOR_JOB_ID>SOME_ID</APPVEYOR_JOB_ID>
</Properties>

Expects build variables $(APPVEYOR_JOB_ID) and $(XUnitTestResults) to be provided.
Uploads the xUnit test results Xml file specified as $(XUnitTestResults) to AppVeyor job $(APPVEYOR_JOB_ID)
#>

[CmdletBinding()]
param ()


function LogInfo([string]$message) {
    $formattedMessage = "$(Get-Date -format 'yyyy-MM-dd HH:mm:sss.fff') [INFO] $message"
    Write-Host $formattedMessage
    if ($env:APPVEYOR) {
        Add-AppveyorMessage -Message $formattedMessage -Category Information
    }
}

function LogError([string]$message) {
    $formattedMessage = "$(Get-Date -format 'yyyy-MM-dd HH:mm:sss.fff') [ERROR] $message"
    Write-Host $formattedMessage -foregroundcolor red
    if ($env:APPVEYOR) {
        Add-AppveyorMessage -Message $formattedMessage -Category Error
    }
}

$myDir = Split-Path -Parent $MyInvocation.MyCommand.Path

$buildPropeties = "$myDir\msbuild-properties.xml"

if (-not (Test-Path $buildPropeties)) {
    LogInfo "Build propeties not found: $buildPropeties"
   return -1
}


# Import email settings from config file
[xml]$BuildPropertiesFile = Get-Content $buildPropeties

$appVeyorJobId = $BuildPropertiesFile.Properties.APPVEYOR_JOB_ID
$xUnitResults = $BuildPropertiesFile.Properties.XUnitTestResults

if ($appVeyorJobId) {
    # upload results to AppVeyor
    $wc = New-Object 'System.Net.WebClient'
    $endPoint = "https://ci.appveyor.com/api/testresults/xunit/$appVeyorJobId"
    $wc.UploadFile($endPoint, (Resolve-Path $xUnitResults))
    LogInfo "$(Resolve-Path $xUnitResults) uploaded to $endPoint"
} else {
    LogError "$xUnitResults not uploaded. APPVEYOR_JOB_ID not provided"
}

