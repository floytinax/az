﻿//-----------------------------------------------------------------------
// <copyright file="Corpus.cs" company="Floytinax">
//     Copyright (c) Floytinax. All rights reserved.
// </copyright>
// <author>Sergey Axenov</author>
//-----------------------------------------------------------------------
namespace NFlax.Az.Core
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents text corpus
    /// </summary>
    public class Corpus
    {
        /// <summary>
        /// Corpus file system
        /// </summary>
        private readonly ICorpusFileSystem fs;

        /// <summary>
        /// Initializes a new instance of the <see cref="Corpus"/> class
        /// </summary>
        /// <param name="fs">Corpus file system</param>
        public Corpus(ICorpusFileSystem fs)
        {
            this.fs = fs;
        }

        /// <summary>
        /// Reads a raw source of a book in corpus
        /// </summary>
        /// <param name="name">Book name</param>
        /// <returns>Content of the book</returns>
        public string ReadBookSource(string name)
        {
            return this.fs.ReadAllText(Path.Combine("src", name));
        }
    }
}
