﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFlax.Az.Core
{
    public class CorpusFileSystem: ICorpusFileSystem
    {
        private string _root;

        public CorpusFileSystem(string root)
        {
            _root = root;
        }
        public string ReadAllText(string path)
        {
            return File.ReadAllText(Path.Combine(_root, path));
        }
    }
}
