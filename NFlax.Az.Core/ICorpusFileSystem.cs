﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFlax.Az.Core
{
    /// <summary>
    /// Defines a set of IO operations used by Corpus
    /// </summary>
    public interface ICorpusFileSystem
    {
        string ReadAllText(string path);
    }
}
