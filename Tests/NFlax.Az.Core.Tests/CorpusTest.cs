﻿namespace NFlax.Az.Core.Tests
{
    using System.IO;
    using FluentAssertions;
    using NSubstitute;
    using Xunit;

    public class CorpusTest
    {
        public class ReadBookSource
        {
            [Fact(DisplayName="Given book exists as a file When the book name provided Then the corpus returns book content")]
            public void ExistingName_ReturnSourceContent()
            {
                var bookName = "book1.txt";
                var bookContent = "Text content";

                var fs = Substitute.For<ICorpusFileSystem>();
                fs.ReadAllText(Path.Combine("src",bookName)).Returns(bookContent);

                var corpus = new Corpus(fs);
                corpus.ReadBookSource(bookName).Should().Be(bookContent);
            }
        }
    }
}
